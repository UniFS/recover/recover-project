# https://docs.docker.com/storage/bind-mounts/#use-a-read-only-bind-mount
# https://linuxize.com/post/how-to-transfer-files-with-rsync-over-ssh/

fs='320-370'
bacstore='/zfsraid0'
to='/Users/uni/Desktop/Nicole_fs'
port=10.200.101.132
usr='uni'

connect(){
  ssh $usr@$port
}

# connect

stream(){
  rsync -av -P $bacstore/$fs $usr@$port:$to

  dbg
}

dbg(){
  echo $bacstore/$fs
}

stream
