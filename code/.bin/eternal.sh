#https://docs.docker.com/storage/bind-mounts/#use-a-read-only-bind-mount

# User's vars
to_check='Now'
bacstor='/run/media/me/try/rsync'

screenr(){
  screen -S eternal # auto increment
}

build(){
  flavor='ubu'
  DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
  cd $DIR
  sudo docker build -f "../$container/Dockerfile" -t $container:$flavor .

  dbg(){
    cd $DIR
    echo "where am I `pwd`"
    ls "../eternal/"
    sudo docker images
  }
  # dbg
  #https://stackoverflow.com/questions/59895/how-to-get-the-source-directory-of-a-bash-script-from-within-the-script-itself
}

immutable(){
  # The src director is the same as $container! i.e. ~/.Uni/recovery/eternal/
export container='eternal'
export eternal="$bacstor/.eternal"
export overlay='fs.eternal'
}

attach(){
  sudo docker container attach $container
}

setup_readonly(){
  mkdir -p $eternal

  #  --rm
  sudo docker run --rm -d \
    -it \
    --name $container \
    --mount type=bind,source=$bacstor/$to_check/,target=/$to_check,readonly \
    --mount type=bind,source=$eternal,target=/$overlay \
    $container:$flavor
}

overlay_csum(){
  # Positions $to_check inside of Overlay so Function( Checksum ) can see it!
  sudo docker exec -it $container sh -c "ln -srf /$to_check /$overlay/"
  #https://docs.docker.com/engine/reference/commandline/exec/
}

clean(){
  sudo docker rm -f $container $name
}

stat_efs(){
  sudo docker ps -a
}

immutable
stat_efs
build  #-- make this Conditional!

main(){
  echo
  setup_readonly
  overlay_csum
  attach
}
main
