# https://unix.stackexchange.com/questions/48298/can-rsync-resume-after-being-interrupted

to='/media/me/try/rsync/all'
partial='/media/me/try/rsync/partial'

recover="sudo rsync -av --partial --partial-dir=$partial --progress"

pre(){
mkdir -p $to $partial
echo 'done setup'
}

all(){
$recover /media/me/LilOleMe\ Backup\ 004/Africa\ Digitize\ 060218 /media/me/try/rsync/
}
